from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
import json
import base64


from snippets.models import Snippet
from snippets.serializers import SnippetSerializer, UserSerializer

# Create your tests here.

class RegistrationTestCase(APITestCase):

    def test_registration(self):
        data = {"username":"test", 
        "password":"test123",
        "email":"",
        "first_name":"",
        "last_name":""}
        response = self.client.post("/users/register",data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

class usersViewTestCase(APITestCase):
    list_url = reverse("user-list")

    def setUp(self):
        self.user = User.objects.create_user(username="davinci",password="davinci123")
        credentials = base64.b64encode('davinci:davinci123'.encode()).decode()
        self.client.credentials(HTTP_AUTHORIZATION='Basic ' + credentials)

    def test_user_list_auth(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_list_un_auth(self):
        self.client.force_authenticate(user=None)
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_detail_retrieve(self):
        response = self.client.get(reverse("user-detail", kwargs={"pk":1}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["username"],"davinci")
    
    def test_user_update_by_owner(self):
        response = self.client.put(reverse("user-detail",kwargs={"pk":1}),{"username":"pavaroti"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content),
            { "url": "http://testserver/users/1/",
            "id": 1,
            "username": "pavaroti",
            "snippets": [],
            "snippetsz": "http://testserver/users/1/snippetsz/"}
        )

    def test_user_delete_by_owner(self):
        response = self.client.delete(reverse("user-detail",kwargs={"pk":1}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_update_by_random_user(self):
        random_user = User.objects.create_user(username="random",password="random123")
        credentials = base64.b64encode('random:random123'.encode()).decode()
        self.client.credentials(HTTP_AUTHORIZATION='Basic ' + credentials)
        response = self.client.put(reverse("user-detail",kwargs={"pk":1}),{"username":"piccaso"})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_user_delete_by_random_user(self):
        random_user = User.objects.create_user(username="random",password="random123")
        credentials = base64.b64encode('random:random123'.encode()).decode()
        self.client.credentials(HTTP_AUTHORIZATION='Basic ' + credentials)
        response = self.client.delete(reverse("user-detail",kwargs={"pk":1}))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class snippetViewTestCase(APITestCase):
    list_url = reverse("snippet-list")

    def setUp(self):
        self.user = User.objects.create_user(username="davinci",password="davinci123")
        credentials = base64.b64encode('davinci:davinci123'.encode()).decode()
        self.client.credentials(HTTP_AUTHORIZATION='Basic ' + credentials)
        snippetInsert = self.client.post(self.list_url, {
            "title": "TestCode",
            "code": "Ovo je test snippet",
            "linenos": "false",
            "language": "java",
            "style": "friendly"
        })


    def test_snippet_list_auth(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_snippet_list_un_auth(self):
        self.client.force_authenticate(user=None)
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_snippet_post(self):
        response = self.client.post(self.list_url, {
            "title": "JAVACode",
            "code": "System.out.println('Hello FER');",
            "linenos": "false",
            "language": "java",
            "style": "murphy"
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_snippet_post_un_auth(self):
        self.client.force_authenticate(user=None)
        response = self.client.post(self.list_url, {
            "title": "JAVACodee",
            "code": "System.out.println('Hello FER');",
            "linenos": "false",
            "language": "java",
            "style": "murphy"
        })
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    
    def test_snippet_detail_retrieve(self):
        response = self.client.get(reverse("snippet-detail", kwargs={"pk":1}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["title"],"TestCode")

    def test_snippet_detail_retrieve_un_auth(self):
        self.client.force_authenticate(user=None)
        response = self.client.get(reverse("snippet-detail", kwargs={"pk":1}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["title"],"TestCode")

    def test_snippet_update_auth(self):
        #response = self.client.put(reverse("snippet-detail",kwargs={"pk":1}),{"style":"abap"})
        response = self.client.patch(reverse("snippet-detail",kwargs={"pk":1}),{"title":"ChangedTestCode"})
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content),
            {   "url": "http://testserver/snippets/1/",
                "id": 1,
                "highlight": "http://testserver/snippets/1/highlight/",
                "owner": "davinci",
                "title": "ChangedTestCode",
                "code": "Ovo je test snippet",
                "linenos": False,
                "language": "java",
                "style": "friendly"}
        )
    def test_snippet_update_un_auth(self):
        self.client.force_authenticate(user=None)
        #response = self.client.put(reverse("snippet-detail",kwargs={"pk":1}),{"style":"abap"})
        response = self.client.patch(reverse("snippet-detail",kwargs={"pk":1}),{"title":"ChangedTestCode"})   
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_snippet_delete_auth(self):
        #response = self.client.put(reverse("snippet-detail",kwargs={"pk":1}),{"style":"abap"})
        response = self.client.delete(reverse("snippet-detail",kwargs={"pk":1}))
        
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        
    def test_snippet_delete_un_auth(self):
        self.client.force_authenticate(user=None)
        #response = self.client.put(reverse("snippet-detail",kwargs={"pk":1}),{"style":"abap"})
        response = self.client.delete(reverse("snippet-detail",kwargs={"pk":1}))   
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
    def test_users_snippet_list_retrieve_auth(self):
        response = self.client.get(reverse("snippetz-detail", kwargs={"pk":1}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"],1)
    
    def test_users_snippet_list_retrieve_un_auth(self):
        self.client.force_authenticate(user=None)
        response = self.client.get(reverse("snippetz-detail", kwargs={"pk":1}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"],1)
        




    

